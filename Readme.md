# Head Hunter
## API for finding jobs and creating vacancies

### Technologies
-  FastAPI, SQLAlchemy, Pydantic
-  PostgreSQL
-  Redis
-  Celery
-  RabbitMQ
-  S3 storage
-  Black, Autoflake